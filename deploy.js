const path = require('path');
const FtpDeploy = require('ftp-deploy');
const ftpDeploy = new FtpDeploy();

const buildPath = path.join(__dirname, '..', 'noob__ui/dist');

const log = (...args) => {
	console.log('→', ...args);
};

const config = {
	user: 'cloud',
	password: 'L0y4Y8z4Q6j5E3t0',
	host: 'ftp://ven.hellem.ru',
	port: 21,
	localRoot: buildPath,
	remoteRoot: './',
	include: ['*', '**/*'],
	exclude: ['dist/**/*.map', 'node_modules/**', 'node_modules/**/.*'],
	deleteRemote: false,
	forcePasv: true
};

// use with promises
ftpDeploy
	.deploy(config)
	.then(res => log('📦', ' deploy finish', res))
	.catch(err => log('📦', ' deploy error', err));

ftpDeploy.on('uploading', data => {
	log('‌📦', ` uploading ${parseInt((data.transferredFileCount / data.totalFilesCount) * 100)}% (${data.transferredFileCount}/${data.totalFilesCount})`, data.filename);
});
